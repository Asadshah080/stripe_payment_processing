class StripeService
  def self.create_card(current_user,stripeToken)

      if current_user.stripe_id.nil?
        customer = Stripe::Customer.create({"email": current_user.email})
        #here we are creating a stripe customer with the help of the Stripe library and pass as parameter email.
        current_user.update(:stripe_id => customer.id)
        #we are updating current_user and giving to it stripe_id which is equal to id of customer on Stripe
      end

      card_token = stripeToken
      #it's the stripeToken that we added in the hidden input
      if card_token.nil?
        redirect_to billing_path, error: "Oops"
      end
      #checking if a card was giving.

      customer = Stripe::Customer.new current_user.stripe_id
      customer.source = card_token
      #we're attaching the card to the stripe customer
      customer.save
      #redirect_to success_path
      OpenStruct.new(success: true)
      rescue
      OpenStruct.new(success: false, error: 'OOPs something happened')



  end

end
